#!/usr/bin/python
# -*- coding: utf-8 -*-

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
    Script générant un fichier au format iCalendar à partir
    d'un emploi du temps CELCAT au format HTML.

    NOTES : - Génère l'emploi du temps du groupe M1INFDC - TDA1 et TPA11
            - Nécessite un accès à Internet

    Licence : CC - BY NC SA, voir http://creativecommons.fr/licences/
--------------------
    Sortie :
        - Un fichier NOM_FICHIER_ICS_SORTIE.ics dans le répertoire courant du script. 

    Dépendances :
        - icalendar : http://icalendar.readthedocs.org/
        - lxml : http://lxml.de/

    Auteur :
        - 2015 : Damien Wojtowicz (damien.wojtowicz@gmail.com)
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

"""
    Imports
"""
import lxml.etree as etree
import urllib.request
from icalendar import Calendar, Event, vDatetime
import datetime
import os


"""
    Variables 'globales'
"""

# URL de la feuille XML de l'emploi du temps
URL_EDT_XML = "https://edt.univ-tlse3.fr/FSI/FSImentionM/Info/g30705.xml"

# URL de la feuille de style XSL
URL_EDT_XSLT = "https://edt.univ-tlse3.fr/FSI/FSIpargroupes/gridttss.xsl"

# Nom de la promo, du groupe de TD et de TP
NOM_PROMO = "M1 INF-DC"
NOM_TD = "TDA1"
NOM_TP = "TPA11"
NOM_CM = "CMA"

# Date du début du semestre
DEBUT_SEMESTRE = "20160108T000000"

# Nom du calendrier
NOM_CALENDRIER = "EDT M1 DC - UPS"

# Nom du fichier .ics en sortie
NOM_FICHIER_ICS_SORTIE = "m1dctd1tp11.ics"

# Débugage
DEBUG = False

"""
    Programme principal
"""

# Télécharge l'edt au format XML et sa feuille de style XSLT
urllib.request.urlretrieve(URL_EDT_XML, "xml.xml")
urllib.request.urlretrieve(URL_EDT_XSLT, "xslt.xsl")


# Conversion du XML en HTML avec la feuille de style XSLT
dom = etree.parse("xml.xml")
xslt = etree.parse("xslt.xsl")
transform = etree.XSLT(xslt)
hachetml = str(transform(dom))

if DEBUG:
    print(hachetml)
    print("\n\n\n\n\n")

# Suppression des deux fichiers téléchargés
os.remove("xml.xml")
os.remove("xslt.xsl")

# Transforme le HTML en dictionnaire
hachetml = hachetml[hachetml.index("<table border=\"3\">"):hachetml.index("<br></span><footer>")]
semaines = hachetml.split("<tr><th colspan=\"53\"><a target=\"_self\" ")

# Création du calendrier
calendrier = Calendar()
calendrier['dtstart'] = DEBUT_SEMESTRE
calendrier['summary'] = NOM_CALENDRIER
calendrier['X-WR-TIMEZONE'] = "Europe/Paris"

if DEBUG:
    print(hachetml)
    print("\n\n\n\n\n\nNBSEMAINES")
    print(len(semaines))
    print("\n\n\n\n\n\nSEMAINE0")
    print(semaines[0])
    print("\n\n\n\n\n\nSEMAINE1")
    print(semaines[1])
    print("\n\n\n\n\n\nSEMAINE15")
    print(semaines[15])
    print("\n\n\n\n\n\n")

# Parcours des semaines
for semaine in semaines:
    
    lignes_semaine = semaine.split("\n")
    is_debut_semaine = True
    is_event_en_cours = False
    is_heure_ligne_suivante = False
    is_matiere_ligne_suivante = False
    is_groupe_ligne_suivante = False
    is_salle_ligne_suivante = False
    is_cours_pour_moi = False
    date_courante = datetime.datetime(2000, 1, 1)
    type_cours = ""

    # Parcours de la semaine
    for ligne in lignes_semaine:

        # Récupération de la date du lundi de la semaine
        if "Semaine commençant le" in ligne:
            if DEBUG:
                print(ligne)

            ligne = ligne[ligne.index("ant le ") + 7:ligne.index("ant le ") + 17]
            ligne_date_debut_semaine = ligne.split("/")
            date_courante = datetime.datetime(int(ligne_date_debut_semaine[2]), int(ligne_date_debut_semaine[1]),
                                              int(ligne_date_debut_semaine[0]))

        # Changement de jour
        elif "class=\"reverseday" in ligne:
            date_courante += not is_debut_semaine and datetime.timedelta(days=1) or datetime.timedelta(days=0)
            is_debut_semaine = False

        # Quand il y a un cours, on crée un événement
        elif ("<a name=\"ev" in ligne) and ("\"></a><table>" in ligne):
            cours = Event()
            is_event_en_cours = True
            is_cours_pour_moi = False

            if DEBUG:
                print("Début cours")

        # Analyse de l'heure
        elif "Heures:" in ligne:
            is_heure_ligne_suivante = True

        elif is_heure_ligne_suivante and ("<td>" in ligne):
            if DEBUG:
                print(ligne)

            date_tmp = vDatetime(date_courante).to_ical()
            date_tmp = date_tmp.decode('utf-8')
            date_debut = date_tmp.replace('T0000', 'T'+ligne[4:6]+ligne[7:9])
            date_fin = date_tmp.replace('T0000', 'T'+ligne[10:12]+ligne[13:15])
            date_debut = vDatetime.from_ical(bytes(date_debut, 'utf-8'))
            date_fin = vDatetime.from_ical(bytes(date_fin, 'utf-8'))

            # On détermine le type de cours (COURS / TD / EXAM...)
            type_cours = ligne[16:-5] + " "
            is_heure_ligne_suivante = False

        # Analyse de la matière
        elif "Matière:" in ligne:
            is_matiere_ligne_suivante = True

        elif is_matiere_ligne_suivante and ("<td>" in ligne):
            is_matiere_ligne_suivante = False
            type_cours += ligne[11:-7]

            if DEBUG:
                print(type_cours)

        # Test pour savoir si mon groupe a cours
        elif "Groupe" in ligne:
            is_groupe_ligne_suivante = True

        elif is_groupe_ligne_suivante and ("<nobr>" in ligne):
            if NOM_PROMO in ligne:
                if NOM_CM in ligne:
                    is_groupe_ligne_suivante = False
                    is_cours_pour_moi = True

                elif NOM_TD in ligne:
                    is_groupe_ligne_suivante = False
                    is_cours_pour_moi = True

                elif NOM_TP in ligne: 
                    is_groupe_ligne_suivante = False
                    is_cours_pour_moi = True
            
            if DEBUG:
                print(ligne)
                print(is_cours_pour_moi)

        elif is_groupe_ligne_suivante and ("</td>" in ligne):
            is_groupe_ligne_suivante = False

        # Analyse de la salle
        elif "Salle:" in ligne:
            is_salle_ligne_suivante = True

        elif is_salle_ligne_suivante and ("<nobr>" in ligne):
            is_salle_ligne_suivante = False

            if "EXAMEN" in type_cours:
                salle_cours = "Voir secretariat"
            else :
                salle_cours = ligne[11:-7]

        # Le cours est ajouté au calendrier...
        elif is_event_en_cours and ("</table>" in ligne): 
           
            # ...seulement si on a cours
            if is_cours_pour_moi:
                cours.add('dtstart', date_debut)
                cours.add('dtend', date_fin)
                cours.add('summary', type_cours)
                cours.add('location', salle_cours)

                foo = cours['dtstart'].to_ical()
                cours['dtend'].to_ical()
                calendrier.add_component(cours)

                if DEBUG:
                    print(type_cours)
                    print(date_debut)
                    print(salle_cours)
                    print(foo)

            
            is_event_en_cours = False
            is_cours_pour_moi = False
            type_cours = ""

# Ecriture dans le fichier ics
with open(NOM_FICHIER_ICS_SORTIE, 'w') as f:
    calendrier = calendrier.to_ical()
    calendrier = calendrier.decode('utf-8')

    if DEBUG:
        print("\n\n\n\n")
        print(calendrier)
        
    f.write(calendrier)