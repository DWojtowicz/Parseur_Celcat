Script générant un fichier au format iCalendar à partir
d'un emploi du temps CELCAT au format HTML.

NOTES : 
    - Génère l'emploi du temps du groupe M1INFDC - TDA1 et TPA11
    - Nécessite un accès à Internet

Licence :
    - CC - BY NC SA, voir http://creativecommons.fr/licences/

Sortie :
    - Un fichier NOM_FICHIER_ICS_SORTIE.ics dans le répertoire courant du script. 

Dépendances :
    - Python 3
    - icalendar : http://icalendar.readthedocs.org/
    - lxml : http://lxml.de/
    
Exécution :
    python3 edt_to_ics.py

Auteur :
    - 2015 : Damien Wojtowicz (damien.wojtowicz@gmail.com)